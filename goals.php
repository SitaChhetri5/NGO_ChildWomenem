 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Goals & Objective</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">About Us</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="img/about_inner.jpg">
      </div>
      <div class="col-lg-6">
        
        <ul class="list sec-title">
          <h1>Our Goals </h1>
          <span class="line"></span>
                  <li>Support the focus groups to improve the level of education, economic status and increased access to health services through technical support, capacity enhancement and advocacy. 
                  </li>
                  <li>Support the target groups to ensure social rights and increased access to legal services through awareness, advocacy and capacity enhancement</li>
                  <li>Increase the identity of the organization at national level with adequate physical, financial and human resources. 
                  </li>
                  <h1>Objectives</h1>
                  <span class="line"></span>
                  <li>To motivate women, children and illiterate youth towards formal and informal education and provide maternity and nutrition education and also advocate for their right to education. </li>
                  <li>To ensure the living standard of target group through technical support by utilizing local resources, training for skill transfer to create self-employment and provide support to increase access to resources, access of market etc. 
                  </li>
                  <li>To create awareness through training and other medium on women rights, legal provision, human right and increase the level of awareness of awareness on women violence through access to legal services. </li>
                  <li>To develop skilled manpower by providing capacity enhancement activity. 
                  </li>
                  <li>Maximum management of internal and external resources. </li>
                  <li>Good rapport building and coordination with government and national and international non-government organization. 
                  </li>
                  
                  
                </ul>
      </div>
    </div>
  </div>
  </div>

 <?php include('footer.php');?> 