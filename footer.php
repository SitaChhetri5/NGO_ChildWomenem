 <!--footer -->
  <footer>
      <div class="container-fluid">
        <div class="footer">
          <div class="row">
            <div class="col-md-5  col-sm-12">
              <h2>Networking and Affiliation:</h2>
              <ul>
                <li><a href="#">Member of NANGAN</a></li>
                <li><a href="#">Member of Women Human Rights Defender Forum (WHRDF)</a></li>
                <li><a href="#">Member of Gender Network</a></li>
                <li><a href="#">Member of Civil Society Network for Peace</a></li>
                <li><a href="#">Member of Education Committee</a></li>
                <li><a href="#">Member of Food Network </a></li>
                <!-- <li><a href="#">Member of HIV/AIDS struggle committee </a></li> -->
              </ul>
            </div>
            <div class="col-md-4  col-sm-12">
              <h2>Recent Post</h2>
              <div class="recent_post clearfix">
                <div class="wrap_post">
                  <div class="thumb_img">
                  <a href="#"><img src="img/thumb.jpg"></a>
                </div>
                <div class="post_info">
                  <h5>Donation food for childrens</h5>
                  <h6>1 Dec,2017</h6>
                </div>
                </div>
                <div class="wrap_post">
                  <div class="thumb_img">
                  <a href="#"><img src="img/thumb.jpg"></a>
                </div>
                <div class="post_info">
                  <h5>Donation food for childrens</h5>
                  <h6>1 Dec,2017</h6>
                </div>
                </div>
                 <div class="wrap_post no-boredr">
                  <div class="thumb_img">
                  <a href="#"><img src="img/thumb.jpg"></a>
                </div>
                <div class="post_info">
                  <h5>Donation food for childrens</h5>
                  <h6>1 Dec,2017</h6>
                </div>
                </div>
              </div>
            </div>
            <div class="col-md-3  col-sm-12">
              <div class="sitemap">
                <h2>Get In Touch</h2>
                <ul class="footer-contact">
                  <li><i class="fa fa-map-marker"></i>Pokhara Kaski, Nepal </li>
                  <li><i class="fa fa-phone"></i>+977-61-551367</li>
                  <li><i class="fa fa-envelope"></i><a href="#">cwes2052@gmail.com</a></li>
                  <li><i class="fa fa-globe"></i><a href="http://www.cwesn.org.np/">www.cwesn.org.np</a></li>
                </ul>
                 <ul class="social-media">
                <li><a href="#"><i class="fa fa-facebook"></i> </a></li>
                <li><a href="#"><i class="fa fa-twitter"></i> </a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
              </div>
            </div>
          </div>
        <!--   <div class="bottom-footer"> 
            <div class="text-left">
              <small>© 2017. All Right Reserved. <a href="#">Child & Women Empowerment Society Nepal</a></small>
            </div>
              <div class="text-right">
              <small class="">Designed By <a href="http://www.webpagenepal.com">Webpage Nepal</a></small>
            </div>
            </div> -->
            <div class="bottom-footer">
    <div class="container">
      <p>© 2017 Child & Women Empowerment Society Nepal. All Right Reserved.</p>
      <p>Design by<a href="http://www.webpagenepal.com"> Webpage Nepal</a></p>
    </div>
  </div>
          
        </div>
      </div>
  </footer>
  <!--footer-->
  
<!-- </div> -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="bootstrap/js/bootstrap.min.js"></script> 
<!-- Modernizr javascript --> 
<script type="text/javascript" src="plugins/modernizr.js"></script> 
<!-- Jquery Ui javascript --> 
<script src="js/jquery-ui.js"></script> 
<!-- <script src="js/scrolleffect.js"></script>  -->
<!-- jQuery REVOLUTION Slider  --> 
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>  
<script src="js/snap.svg-min.js"></script> 
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script src="js/script.js"></script> 

    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/jquery.appear.js"></script>
 <script type="text/javascript" src="js/animate.js"></script>
   
 

</body>
</html>