 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Norms and Values</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Norms</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="img/about.jpg">
      </div>
      <div class="col-lg-6">
        
        <ul class="list sec-title">
          <h1>Norms and Values</h1>
          <span class="line"></span>
                  <li>Assurance of Right of women and children, Focus for participation to assure empowerment.
                  </li>
                  <li>Non-discrimination on the basis of caste, ethnicity, class, language and culture.</li>
                  <li>Focus on gender equality and non-politics.</li>
                  <li>Devoted and committed to maintain transparency.</li>
                  <li>Good Governance and Accountability in actions.</li>
                  <li>Disaster Management, Reconstruction and Climate Change.</li>
                  
                  
                </ul>
      </div>
    </div>
  </div>
  </div>

 <?php include('footer.php');?> 