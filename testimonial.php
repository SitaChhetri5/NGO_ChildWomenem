 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Testimonial</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Testimonial</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="inner-page padding40 clearfix">
      <ul class="testimonial-wrap">
                <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials"> <span class="name">
            Prakriti Chhetri, Pokhara            </span> <span class="date">
            2017-07-18 00:22:58            </span>
            <p>
              Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.            </p>
          </div>
        </li>
        <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials"> <span class="name">
            Prakriti Chhetri, Pokhara            </span> <span class="date">
            2017-07-18 00:22:58            </span>
            <p>
              Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.            </p>
          </div>
        </li>
        <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials"> <span class="name">
            Prakriti Chhetri, Pokhara            </span> <span class="date">
            2017-07-18 00:22:58            </span>
            <p>
              Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.            </p>
          </div>
        </li>
        <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials"> <span class="name">
            Prakriti Chhetri, Pokhara            </span> <span class="date">
            2017-07-18 00:22:58            </span>
            <p>
              Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.Nulla facilisis tellus maximus lectus congue tincidunt. In hac habitasse platea dictumst. Quisque malesuada mi imperdiet, commodo eros sed, aliquet tortor. Aenean id arcu in lacus semper gravida. Proin porta leo et facilisis pretium.            </p>
          </div>
        </li>
           </ul>
      <div class="row">
        <div class="col-md-12">
          <div class="review-form clearfix">
            <h2 class="title">Write your own review</h2>
            <form>
              <div class="form-group">
                <label>Full Name<sup>*</sup></label>
                <input name="txtNickname" id="txtNickname" class="form-control" type="text">
              </div>
              <div class="form-group">
                <label>Address<sup>*</sup></label>
                <input name="txtNickname" id="txtNickname" class="form-control" type="text">
              </div>
               <div class="form-group">
                <label>Your Review</label>
                <textarea class="form-control" name="txtReview" id="txtReview" rows="3"></textarea>
              </div>
              <div class="form-group">
                <!-- <button type="submit" name="" class="btn btn-default">Submit</button> -->
                <button type="submit" class="btn btn-outline">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
 <?php include('footer.php');?> 