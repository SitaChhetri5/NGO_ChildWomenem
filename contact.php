<?php include('header.php');?>  
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Contact Us</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Contact Us</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <section class="service-section">
  <div class="container custom-container">
    <div class="inner-page padding40 clearfix">
      
      <div class="contact-info">
        <div class="row">
          <div class="col-12 col-md-7 col-sm-6">
            <h3>Contact Information</h3>
            <ul>
              <li><i class="fa fa-map-marker"></i> Pokhara Lekhnath-8, Phirke, Kaski, Nepal
                </li>
              <li><i class="fa fa-mobile"></i> +977-61-551367, 525431</li>
              <li><i class="fa fa-envelope"></i> cwes2055@gmail.com</li>
              <li><i class="fa fa-globe"></i> www.cwesn.org.np </li>
            </ul>
     
          </div>
          <div class="col-12 col-md-5 col-sm-6">
            <h3>Get In Touch</h3>
            <p class="lead">Please fill up the required(<span class="red-text">*</span>) field</p>
            <form class="contact-form">
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputName" placeholder="Fullname *" required>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address *" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputPhoneNumber" placeholder="Your phone number">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputSubject" placeholder="Subject">
              </div>
              <div class="form-group">
                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Message *" required></textarea>
              </div>
              
              
             
              <button type="submit" class="btn btn-outline">Submit</button>
            </form>
          </div>
        </div>
      </div>
      <div class="map_canvas">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14063.327112053363!2d83.9778671!3d28.2124231!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe707408457536c4!2sChild+and+Women+Empowerment+Society+Nepal!5e0!3m2!1sen!2snp!4v1513930466281" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>
 <?php include('footer.php');?>