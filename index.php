<?php include('header.php');?>
 <!-- banner start --> 
  <!-- ================ -->
  <div class="banner"> 
    <!-- slideshow start --> 
    <!-- ================ -->
    <div class="slideshow"> 
      <!-- slider revolution start --> 
      <!-- ================ -->
      <div class="slider-banner-container">
        <div class="slider-banner">
          <ul>
            <!-- slide 1 start -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="NGO"> 
              <!-- main image --> 
              <img src="img/banner.jpg"  alt="slidebg1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                            
            </li>
            <!-- slide 1 end --> 
            <!-- slide 2 start -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="NGO"> 
              <!-- main image --> 
              <img src="img/banner2.jpg"  alt="slidebg1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
             
            </li>
            <!-- slide 2 end -->
            
          </ul>
          <div class="tp-bannertimer tp-bottom"></div>
        </div>
      </div>
      <!-- slider revolution end --> 
    </div>
    <!-- slideshow end -->
  <!--   <div class="booking-form">
      <div class="book-title">
        <h2>Donate Now</h2>
        <p>Be a part of the world by make donation</p>
      </div>
      <form method="post" action="booking_form.php" class="book-form">
    <div class="form-group"> <i class="a fa-user"></i>
          <input type="text" name="txtBookedName" class="form-control" placeholder=" Name...">
        </div>
       <div class="form-group"> <i class="fa fa-envelope"></i>
          <input type="text" name="txtBookedEmail" class="form-control" placeholder=" Email...">
        </div>
        <div class="form-group"> <i class="fa fa-map-marker"></i>
          <input type="text" name="txtBookedArrivalDate" class="form-control" placeholder=" City...">
        </div>
        
        <div class="form-group">
           <input type="text" name="amount" class="form-control" placeholder="Amount...">
        </div>
        
        <button type="submit" class="btn btn-warning">Donate</button>
      </form>
    </div> -->
  </div>
  <!-- banner end -->
   <!--ABOUT  -->
  <div class="AboutUs">
    <div class="container-fluid">
      <div class="title text-center">
        <h2>Child and Women Empowerment</h2>
        <div class="seperator"><span class="square-box"></span></div>
        
      </div>
      <div class="row">
            <div class="col-lg-6 pad0" data-animation-effect="fadeInLeft" data-effect-delay="400">
              <div class="about_img">
                <img src="img/about.jpg">
              </div>
            </div>
            <div class="col-lg-6 pad0" data-animation-effect="fadeInRight" data-effect-delay="400">
             <div class="about_bgimg main_btn">
               <div class="about_info">
                <h3>Introduction:</h3>
                <p>
                  Child and Women Empowerment Society is a non-profit and non-government organization established in January, 1999 by a group of 7 young woman from Pokhara of Kaski District. The organization started its existence as Base Youth Club with focus on making youths responsible toward minimizing environmental problems along with advocacy on issues of woman and necessary interventions for the justice to women. Later on, the group felt high necessity of immediate interventions on children and women toward promotion of their education and level of awareness on issues of health, environment and legal provisions thus making them independent and swabalamban. It is therefore Base Youth Group changed its name to Child and Women Empowerment Society and started working broadly on the issues of women and children.

                </p>
                <a href="intro.php" class="btn btn-outline">Read More</a>
               </div>
             </div>
            </div>
      </div>
    </div>
  </div>
  <!--ABOUT  -->
    <!--Causes  -->
  <div class="Causes">
    <div class="container-fluid">
      <div class="title text-center">
        <h2>Our Works</h2>
        <div class="seperator"><span class="square-box"></span></div>
        
      </div>
      <div class="row">
        <div class="col-lg-3" data-animation-effect="fadeInLeft" data-effect-delay="100">
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/cause1.jpg">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> Naya Bato Naya Paila</h3>
                <p class="text-center"> The action intervened on the sector of Commercial Sexual Exploitation on children in coordination with government offices and line agencies. </p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Project Amount : </span>346,9886.03</li>
                     <li> <span>Donor : </span>World Education Incorporated</li>
                      <li> <span>Duration : </span>15-Jul-2011 to 30-Sep-2012</li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
        <div class="col-lg-3" data-animation-effect="fadeInLeft" data-effect-delay="100">
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/cause2.jpg">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> Local Rights Program</h3>
                <p class="text-center"> Based on Human Rights Based Approach, the action focuses upon land rights, good governance, gender equality & social inclusion and child education. </p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Project Amount :</span>34,453,616</li>
                     <li> <span>Donor : </span>Action Aid Nepal</li>
                      <li> <span>Duration :</span>2005 – Ongoing</li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
        <div class="col-lg-3" data-animation-effect="fadeInLeft" data-effect-delay="200">
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/cause3.jpg">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> HIV Prevention Program</h3>
                <p class="text-center"> The action is more focused upon the prevention of STIs and HIV through awareness apart from services from Drop In center & Community Information Center  </p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Project Amount :</span>33,679,339</li>
                     <li> <span>Donor : </span>USAID funded through FHI 360</li>
                      <li> <span>Duration :</span>2007 – Ongoing</li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
       <div class="col-lg-3" data-animation-effect="fadeInLeft" data-effect-delay="300">
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/cause4.jpg">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> Early Child Development Project</h3>
                <p class="text-center">The project supported on infrastructural as well as capacity building aspect for quality education at ECDC in 3 VDCs of the district.  </p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Project Amount:</span>11,896,215.19</li>
                     <li> <span>Donor : </span>Guy Trust through Action Aid Nepal</li>
                      <li> <span>Duration :</span>Sep–2014 to Jan–2017</li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Causes-->

  <!--Testimonial  -->
    <div class="section-content parallax-bg" data-animation-effect="pulse" data-effect-delay="300">
      <div class="testimonial-wrapper">
          <div class="title text-center testi">
        <h2>Donor Speak</h2>
        <div class="seperator sep_testi"><span class="square-box"></span></div>
        
      </div>
        <div class="owl-carousel owl-theme">
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
         <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
        </div>
      </div>
    </div>
  <!--Testimonial-->
   <?php include('footer.php')?>