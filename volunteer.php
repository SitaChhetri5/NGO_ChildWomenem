 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent page-top_volunter">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Become a Volunteer</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Volunteer</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
 <!-- start volunter -->
   <section class="volunter">
    <div class="container">
      <div class="row">
       <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <figure>
          <div class="img"><a href="#"><img src="img/volunteer_img.jpg" alt="img"></a></div>
            <figcaption>
              <div class="overlay">
                <h3><a href="#">Prakriti Chhetri</a></h3>
                <ul class="social_link">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li ><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li ><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </figcaption>
        </figure>
       </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <figure>
          <div class="img"><a href="#"><img src="img/volunteer_img.jpg" alt="img"></a></div>
           <figcaption>
              <div class="overlay">
                <h3><a href="#">Prakriti Chhetri</a></h3>
                <ul class="social_link">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li ><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li ><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </figcaption>

        </figure>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
       <figure>
        <div class="img"><a href="#"><img src="img/volunteer_img.jpg" alt="img"></a></div>
          <figcaption>
              <div class="overlay">
                <h3><a href="#">Prakriti Chhetri</a></h3>
                <ul class="social_link">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li ><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li ><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </figcaption>

      </figure>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <figure>
        <div class="img"><a href="#"><img src="img/volunteer_img.jpg" alt="img"></a></div>
          <figcaption>
              <div class="overlay">
                <h3><a href="#">Prakriti Chhetri</a></h3>
                <ul class="social_link">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li ><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li ><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </figcaption>

      </figure>
    </div>
</div>
<div class="mid-header">
  <!-- Trigger the modal with a button -->

  <div class="row">
      <div class="col-lg-12">
        <h3 class="form-heAD">Volunteer Application Form</h3>
        <div id="divRegistrationForm" class="registration_form">
      <div class="inner_wrapper">
        
     
     <form class="form-res clearfix">      
          <div class="row clearfix">

            <div class="col-md-12 text-right">
              <input id="hdnPhoto" name="hdnPhoto" value="" type="hidden">
              <div id="divUploadImage" class="upload_img pull-right">
                <span>Upload Photo</span>
                <input id="txtUploadPhoto" name="txtUploadPhoto" type="file">
              </div>
            </div>
          </div>


          <h4>General Information</h4>


          <div class="form-group">
            <label>Full Name (In block letter):</label>
            <input id="txtFullName" name="txtFullName" class="form-control" type="text">
          </div>
          <div class="form-group">
            <label>Address:</label>
            <input id="txtAddress" name="txtAddress" class="form-control" type="text">
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6 half-width">
                <label>Age:</label>
                <input id="txtAge" name="txtAge" class="form-control" type="text">
              </div>

              <div class="col-md-6 radio half-width">
                <h4>Sex:</h4>
                <input name="rdioSex" id="rdioMale" value="Male" type="radio">
                <label class="radio-inline" for="rdioMale"><span></span>Male</label>
                <input name="rdioSex" id="rdioFemale" value="Female" type="radio">
                <label class="radio-inline" for="rdioFemale"><span></span>Female </label>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Mobile:</label>
                <input name="txtMobile" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Land Line:</label>
                <input name="txtLandline" class="form-control" type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Email:</label>
                <input name="txtEmail" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Social Page Link:</label>
                <input name="txtFB" class="form-control" type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Sponsor:</label>
                <input name="txtSponsor" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Contact No:</label>
                <input name="txtSponsorContact" class="form-control" type="text">
              </div>
            </div>
          </div>
          <h4>Agreement and Signature </h4>  
          <hr>


          <div class="radio full-width">
            <input name="rdioDeclaration" id="rdioDeclaration" value="1" type="checkbox">
            <label class="radio-inline" for="rdioDeclaration"><span></span>By submitting this application, I, the undersigned, affirm that the facts declared in it are true.</label>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">

                <input name="signatureName" class="form-control" type="text">
                <label>Name / Signature of applicant</label>
              </div>
              <div class="col-md-6">
                <label class="text-right">Date:</label>
                <input name="date" class="form-control" type="text">
              </div>

            </div>
          </div>
  <div class="form-group">
            <button type="button" class="btn btn-info btn-lg btn volunteer-btn" >Save </button>
          </div>
        </form>
      
      
      </div>
    </div>

      </div>
  </div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times blue" aria-hidden="true"></i>
</button>
        
      </div>
      <div class="modal-body">
        <div id="divRegistrationForm" class="registration_form">
      <div class="inner_wrapper">
        
     
     <form class="form-res clearfix">      
          <div class="row clearfix">

            <div class="col-md-12 text-right">
              <input id="hdnPhoto" name="hdnPhoto" value="" type="hidden">
              <div id="divUploadImage" class="upload_img pull-right">
                <span>Upload Photo</span>
                <input id="txtUploadPhoto" name="txtUploadPhoto" type="file">
              </div>
            </div>
          </div>


          <h4>General Information</h4>


          <div class="form-group">
            <label>Full Name (In block letter):</label>
            <input id="txtFullName" name="txtFullName" class="form-control" type="text">
          </div>
          <div class="form-group">
            <label>Address:</label>
            <input id="txtAddress" name="txtAddress" class="form-control" type="text">
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6 half-width">
                <label>Age:</label>
                <input id="txtAge" name="txtAge" class="form-control" type="text">
              </div>

              <div class="col-md-6 radio half-width">
                <h4>Sex:</h4>
                <input name="rdioSex" id="rdioMale" value="Male" type="radio">
                <label class="radio-inline" for="rdioMale"><span></span>Male</label>
                <input name="rdioSex" id="rdioFemale" value="Female" type="radio">
                <label class="radio-inline" for="rdioFemale"><span></span>Female </label>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Mobile:</label>
                <input name="txtMobile" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Land Line:</label>
                <input name="txtLandline" class="form-control" type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Email:</label>
                <input name="txtEmail" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Social Page Link:</label>
                <input name="txtFB" class="form-control" type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Sponsor:</label>
                <input name="txtSponsor" class="form-control" type="text">
              </div>
              <div class="col-md-6">
                <label class="text-right">Contact No:</label>
                <input name="txtSponsorContact" class="form-control" type="text">
              </div>
            </div>
          </div>
          <h4>Agreement and Signature </h4>  
          <hr>


          <div class="radio full-width">
            <input name="rdioDeclaration" id="rdioDeclaration" value="1" type="checkbox">
            <label class="radio-inline" for="rdioDeclaration"><span></span>By submitting this application, I, the undersigned, affirm that the facts declared in it are true.</label>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">

                <input name="signatureName" class="form-control" type="text">
                <label>Name / Signature of applicant</label>
              </div>
              <div class="col-md-6">
                <label class="text-right">Date:</label>
                <input name="date" class="form-control" type="text">
              </div>

            </div>
          </div>
  <div class="form-group">
            <button type="button" class="btn btn-info btn-lg btn volunteer-btn" >Save </button>
          </div>
        </form>
      
      
      </div>
    </div>
      </div>
      
    </div>

  </div>
</div>
</div>
</div>
</section>
<!-- End volunter -->

  <?php include('footer.php');?>