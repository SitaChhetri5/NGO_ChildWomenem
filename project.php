 <?php include('header.php');?>    
 <div class="page-top parallax dark-translucent page-top_volunter">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
 <!-- start volunter -->
   <section class="volunter projects_cause">
    <div class="container">
      <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <figure>
          <div class="img">
            <a href="#">
              <img src="img/cause1.jpg" alt="img">
            </a>
          </div>
            <figcaption>
              <div class="overlay">
               <div class="cause_title">
                <h3 class="text-center">
                <a href="local_right_Project1.php">Local Rights Program</a>
                </h3>
                <p class="text-center"> Child and Women Empowerment Society Kaski has been implementing Local Rights Program since 2005.  </p>
              </div>
                <div class="cause_detail recent_cause">
                  <ul>  
                    <li> <span>Donor : </span> Actionaid Nepal</li>
                     <li> <span>Working areas : </span>Bhalam, Kahun and Armala of Pokhara Lekhnath Metropolitan City</li>
                      <li> <span>Duration : </span>2005 - 2017 </li>
                  </ul>
                </div>
              </div>

                <div class="text-center">
                <a href="local_right_Project1.php" class="btn btn-donate">Project Details</a>
              </div>
            </figcaption>
        </figure>
       </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <figure>
          <div class="img">
            <a href="#">
              <img src="img/cause1.jpg" alt="img">
            </a>
          </div>
            <figcaption>
              <div class="overlay">
               <div class="cause_title">
                <h3 class="text-center">
                <a href="linkages.php">LINKAGES</a>
                </h3>
                <p class="text-center"> CWES linkages with other projects through collaboratio and networking with District AIDS Coordination Committee (DACC) </p>
              </div>
                <div class="cause_detail recent_cause">
                  <ul>  
                    <li> <span>Donor : </span> USAID and PEFAR through fhi360</li>
                                   <li> <span>Project Area : </span>Pokhara Lekhnath Metropolitan City and periphery to Prithvi Rajmarg within Kaski district
</li>
                     <li> <span>Target Groups: </span>Most-At-Risk-Population (MARP) (Female Sex Workers and their Clients)</li>
       
                  </ul>
                </div>
              </div>

                <div class="text-center">
                <a href="linkages.php" class="btn btn-donate">Project Details</a>
              </div>
            </figcaption>
        </figure>
       </div>
      
</div>

</div>
</section>
<!-- End volunter -->

 <?php include('footer.php');?> 