 <?php include('header.php');?>   
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent Project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
      <div class="row">
        <div class="list sec-title"">
          <div class="col-lg-6">
          <h1 class=""> LINKAGES</h1>
            <span class="line"></span>
        </div>
        <div class="col-lg-6 text-right">
          <a href="project.php" class="breadcrumb">
          <i class="fa fa-tasks" aria-hidden="true"></i>
Back to Project
        </a>
        </div>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-12">
       
         <ul class="list sec-title">
          
          <p class="details"> 
              CWES is implementing HIV and AIDS prevention and Care program with support of USAID Nepal and PEFAR 2007. 

          </p>
                  
                  <h4 class="margin10">  OBJECTIVES</h4>
                  <ul class="theme">  
                    <li>  Prevent HIV transmission among Most-At-Risk-Population (MARP) (Female Sex Workers and
their Clients) in Pokhara Valley & its periphery </li>
                     <li> To increase access to prevention and care services among the most-at-risk populations.
</li>
                  </ul>
                  <h4 class="margin32">APPROACH</h4>
                  <p class="details"> 
CWES linkages with other projects through collaboratio and networking with District AIDS Coordination Committee (DACC), NGOs, Community Based Organizations (CBOs), District Public Health Office (DPHO),  District Development Committee (DDC), alliance and networks for HIV/AIDS, PLHA support group and other concerned organizations/ stakeholders. Advocacy and local coordination is being helpful to reduce stigma and discrimination towards MARPs and PLHA.

                  </p>
                   <h1> Major Activities</h1>
          <span class="line"></span>
           <h5 class="Activities">Build Capacity of the organization to implement HIV prevention program
 </h5>
 <p class="activities_detail">  This includes capacity building activities (training, workshop, exposure visits, etc.) of Staff, Board Members and Peer Educators. This will be supportive for supervision, monitoring and mentoring.
</p>
 <h5 class="Activities">Implementation of HIV Prevention Program

 </h5>
 <p class="activities_detail">  This includes institutional behavioral communication through community and peer based outreach education using variety of communication channel and media including individually focused health education and Drop-In-Centers (DIC) operation for positive behavior change and maintenance. The education also includes condom promotion, distribution, referral and follow up for STI diagnosis and treatment. This also includes counseling,testing, care, support, treatment and others.

</p>
 <h5 class="Activities">Strengthen Community Mobilization, Coordination, Networking and Advocacy.

 </h5>
 <p class="activities_detail">  This includes coordination, networking meetings with stakeholders, planned group discussions, stigma and discrimination reduction training, celebration of special days etc.

</p>
                </ul>

      </div>
    </div>
  </div>
  </div>

<?php include('footer.php');?> 