 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>News and Events</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">News</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
      <div class="col-lg-9">
        <div class="news">
          <div class="news_img">
            <img src="img/cause2.jpg">
          </div>
          <div class="news_detail">
             <h5><a href="news_detailpage.php">Visit Renu Madam at khaun reflect group</a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"></i>2 Jan Dec,2018</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting </p>
            <div class="read_btn">
              <a href="news_detailpage.php">Read More</a>
            </div>
          </div>
        </div>
        <div class="news">
          <div class="news_img">
            <img src="img/cause3.jpg">
          </div>
          <div class="news_detail">
             <h5><a href="news_detailpage.php">Visit Renu Madam at khaun reflect group</a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"></i>2 Jan Dec,2018</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting  </p>
             <div class="read_btn">
              <a href="news_detailpage.php">Read More</a>
            </div>
          </div>
        </div>
        <div class="news">
          <div class="news_img">
            <img src="img/cause2.jpg">
          </div>
          <div class="news_detail">
            <h5><a href="news_detailpage.php">Visit Renu Madam at khaun reflect group</a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"></i>2 Jan Dec,2018</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</p>
             <div class="read_btn">
              <a href="news_detailpage.php">Read More</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 sidebar">
         <div class="block">
    <h4>News & Events</h4>
    <div class="newslist">
      <ul>
        <li><a href="news_detailpage.php"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="news_detailpage.php"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="news_detailpage.php"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
      </ul>
    </div>
  </div>
      </div>
    </div>
  </div>
  </div>

 <?php include('footer.php');?> 