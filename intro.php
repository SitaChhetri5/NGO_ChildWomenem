 <?php include('header.php');?>   
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Mission & Vission</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">About Us</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="abt_img">
          <img src="img/about.jpg">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="Introduction sec-title">
           <h1>Introduction</h1> 
          <span class="line"></span>
                <p>
                 Child and Women Empowerment Society is a non-profit and non-government organization established in January, 1999 by a group of 7 young woman from Pokhara of Kaski District. The organization started its existence as Base Youth Club with focus on making youths responsible toward minimizing environmental problems along with advocacy on issues of woman and necessary interventions for the justice to women. Later on, the group felt high necessity of immediate interventions on children and women toward promotion of their education and level of awareness on issues of health, environment and legal provisions thus making them independent and swabalamban. It is therefore Base Youth Group changed its name to Child and Women Empowerment Society and started working broadly on the issues of women and children. 
                </p>
         <!--  <h1>Mission</h1> 
          <span class="line"></span>
                <p>
                 Ensure social rights of marginalized women and children with promotion of their education, health and economic growth

                </p>
                <h1>Vision</h1> 
          <span class="line"></span>
                <p>Creation of gender balanced society with social security</p> -->
        </div>
      
      </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6">
           <div class="mission">
                <div class="text-center">
                  <div class="block-number">01</div>
                        <h2><a href="#">Our Mission</a></h2>
                        <div class="title">Ensure social rights of marginalized women and children with promotion of their education, health and economic growth</div>
                  </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6">
           <div class="mission">
                <div class="text-center">
                  <div class="block-number">02</div>
                      <h2><a href="#">Our Vision</a></h2>
                          <div class="title">Creation of gender balanced society with social security</div>
                </div>
            </div>
        </div>
    </div>
  </div>

    <?php include('footer.php');?> 