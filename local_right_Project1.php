 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent Project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
      <div class="row">
        <div class="list sec-title"">
          <div class="col-lg-6">
          <h1 class=""> Local Rights Program</h1>
            <span class="line"></span>
        </div>
        <div class="col-lg-6 text-right">
          <a href="project.php" class="breadcrumb">
         <i class="fa fa-tasks" aria-hidden="true"></i>
Back to Project
        </a>
        </div>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-12">
       
         <ul class="list sec-title">
          <p class="details"> 
              Child and Women Empowerment Society Kaski has been implementing Local Rights Program since 2005. As per the changing context of the communities and the priorities set up by the country strategy paper of Actionaid Nepal, CWES has been working with the communities in various issues. All of the activities implemented were directed by right based approach. Initially, Child and Women Empowerment Society with support from Actionaid Nepal worked on issues of HIV and AIDS in Dhikurpokhari VDC, Kahun VDC and Armala VDC of Kaski. It was from 2009 that right based program was implemented in Kahun VDC and Armala VDC during which food and land rights, women rights, education rights and rights to health were advocated and promoted. From 2012 onwards, local rights program was initiated in Kahun V.D.C. and Armala  V.D.C. being based on Human Rights Based Approach. As directed by the country strategy paper of ActionAid Nepal: 2012 - 2017, CWES Nepal is implementing activities of various kinds to meet the following objectives;

          </p>
                  <li>Ensure improved livelihoods and build climate resilient communities by enabling right holders to claim rights to productive resources.
                  </li>
                  <li>Enable Right Holders and CSOs to hold the state and non state institutions accountable to promote, protect and fulfill human rights while advocating for their decisions, allocations and practices to reflect rights holders' ideas and aspiration.
</li>
                  <li>Enable women and girls to challenge and take actions against all forms of discrimination and injustice to claim control over their bodies.
</li>
                  <li>Support all children and youth to have full attainment of quality education in a safer and equitable environment and help them to develop and grow as key drivers of social transformation for a poverty free society.
</li>
                  
                 <h1 class="pad10">Context:</h1>
                  <p class="details"> 
                      Child and Women Empowerment Society Nepal currently works in 3 wards of Pokhara Lekhnath Metropolitan City of Kaski district. A total of 5647 households exist with total population of 22,939 in the working areas of Local Rights Program. Pokhara sub-metropolitan city has been declared as Pokhara Lekhnath Metropolitan City because of which the working wards of Local Rights Program have been merged with other wards. Armala, which was ward no. 28 of sub-metropolitan city, is merged with ward no. 16 of the municipality  whereas Kahun, which was ward no. 21 of the sub-metropolitan city, is now ward no. 11 of the municipality. Mauja Village Development Committee (VDC) is now merged with Bhalam and is fixed as ward no. 20, same ward number as that it was in Pokhara sub-metropolitan city. The merging of wards has created some sorts of confusion regarding receiving services from the wards in the initial phase. The district is now divided into Pokhara Lekhnath Municipality (with 33 wards) and 4 other Rural Municipalities. 4 rural municipalities have been given their names as Rupa Rural Municipality (with 7 wards), Madi Rural Municipality (with 12 wards), Machhapuchhre Rural Municipality (with 9 wards) and Annapurna Rural Municipality (with 11 wards). Previously, it was divided into Pokhara sub-metropolitan city, Lekhnath Municipality and 32 V.D.Cs.

                  </p>
                  <h4 class="margin3">  Themes</h4>
                  <ul class="theme">  
                    <li> Improved Access to Land and Natural Rources </li>
                     <li> Disaster Management </li>
                      <li> Good Governance </li>
                       <li> Women Rights </li>
                       <li> Child Rights </li>
                  </ul>
                </ul>
      </div>
      
      <div class="col-lg-6">
        
       
      </div>
    </div>
  </div>
  </div>

   <?php include('footer.php');?> 